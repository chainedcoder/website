import React from 'react';
import TopNav from '../components/TopNav'
import '../App.css';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

class Error404 extends React.Component {
  constructor() {
    super();
    this.state = {
      scrolled: true
    };
  }

  render() {
    return (
      <div className='App'>

        <TopNav four0four={true} scrolled={this.state.scrolled}/>
        <div className='error404-body'>
          <Grid container spacing={0}>
            <Grid item sm={5} lg={5} md={5} xs={12}>
              <div className='error-msg'>
              <h1>Stormy!</h1>
              <h2>... even stars fade and colors turn to grey.</h2>
              <p>You could go back to <a href='/'>where you were</a> or head straight to our <a href='/'> homepage</a>.</p>
              </div>
            </Grid>
            <Grid item sm={6} lg={6} md={6} xs={12}>
              <div className='error-image'></div>
            </Grid>
          </Grid>
        </div>
        
        <footer className='footer-section'>
          
          <div className='footer-links'>

            <Grid container justify="center" spacing={16}>
              <Grid item sm={3} lg={3} md={3} xs={12}>
                <div className='footer-logo'></div>
                <h3>Spektra Inc.</h3>
                <p>New York, Nairobi, Accra</p>

              </Grid>
              <Grid item sm={3} lg={3} md={3} xs={12}>
                <h2>QUICK LINKS</h2>
                <a href='/signup'>Create An Account</a>
                <a href='/documentation'>Documentation</a>
                <a href='/support'>My Account</a>

              </Grid>
              <Grid item sm={3} lg={3} md={3} xs={12}>
                <h2>providers</h2>
                <a href='#safaricom'>Safaricom</a>
                <a href='#mtn'>MTN</a>
                <a href='#airtel'>Airtel</a>
                <a href='#tigo'>Tigo</a>
                <a href='#vodafone'>Vodafone</a>

              </Grid>
              <Grid item sm={3} lg={3} md={3} xs={12}>
                <h2>Need help?</h2>
                <a href='#contactus'>Contact Us</a>
                <a href='#support'>Support</a>
                <a href='#blog'>Blog</a>
                <a href='#howto'>How-To</a>
                <a href='#forum'>Forum</a>

              </Grid>

            </Grid>
          </div>
          <div className='footer-copy'>
            <div className='pull-left'>Privacy & terms</div>
            <div className='pull-right'>Spektra Inc &copy; 2018 </div>
          </div>

        </footer>

      </div>

    );
  }

  componentDidMount() {
    this.setState({someKey: 'otherValue'});
  }
  componentWillUnmount = () => {
    document.removeEventListener('scroll', this.throttle)
  }
}

export default Error404;
