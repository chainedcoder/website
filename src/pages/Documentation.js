import React from 'react';
import TopNav from '../components/DocNavBar'
import Grid from '@material-ui/core/Grid';
import {withRouter} from 'react-router-dom';
import {throttle, hasClass, offset, scrollTop, asyncForEach } from '../global/functions';

class Documentation extends React.Component {
  constructor() {
    super();
    this.state={
      activeleft:''
    }
  }

  componentDidMount() {
   
      const that = this
      document
      .querySelectorAll('a[href^="/"]')
      .forEach(anchor => {
        anchor
          .addEventListener('click', function (e) {
            e.preventDefault();
            const link = this.getAttribute("href");
              
            that.props.history.push(link)
          })
      }, that)
  
      document
        .querySelectorAll('a[href^="#"]')
        .forEach(anchor => {
          anchor
            .addEventListener('click', function (e) {
              e.preventDefault();
              const link = this
                .href
                .split('#')[1]
                
  
              const retry = (maxRetries, el, fn) => {
                setTimeout(() => {
                  return fn(el).catch((err) => {
                    if (maxRetries <= 0) {
                      throw err;
                    }
                    return retry(maxRetries - 1, fn);
                  });
  
                }, 100)
  
              }
  
              async function scroll(el) {
                document
                  .querySelector(el.getAttribute('href'))
                  .scrollIntoView({behavior: 'smooth'});
              }
              retry(100, this, scroll)
  
            });
        });





        var $navigationLinks = document
        .querySelectorAll('a[href^="#"]')
        var $sections = document
        .querySelectorAll('.doc-section')

        console.log($sections)
        var $sectionsReversed = Array.from($sections).reverse()
        var sectionIdTonavigationLink = {};

        $sections.forEach(function(el) {
          const el_id = el.getAttribute('id')
          sectionIdTonavigationLink[el_id] = document.querySelector("a[href = '#"+el_id+"']");
          // sectionIdTonavigationLink[$(this).attr('id')] = $('#navigation > ul > li > a[href=#' + $(this).attr('id') + ']');
        });

        

        function cachingObjects() {
          var scrollPosition = scrollTop();
      
          $sections.each(function() {
              var sectionTop = offset(this).top;
              var id = this.getAttribute('id');
      
              if (scrollPosition >= sectionTop) {
                  // $navigationLinks.classList.remove('active');
                  // for (var i = 0; i < $navigationLinks.length; i++) {
                  //   $navigationLinks[i].classList.remove('active')
                  // }
                  async function removeClasses(els) {
                    for (var i = 0; i < els.length; i++) {
                      await els[i].classList.remove('active')
                    }
                  }

                  removeClasses($navigationLinks)
                  console.log(id, "this item is active")
                  $navigationLinks.filter('[href=#' + id + ']').classList.add('active');
              }
          });
        }

      


        function optimized() {
          var scrollPosition = scrollTop()+100;
      
          $sections.forEach(function(section) {

            var currentSection = section;
              // var sectionTop = currentSection.offsetTop;
              if (scrollPosition >= section.offsetTop&& scrollPosition <= section.offsetTop + section.clientHeight) {
                  var id = currentSection.getAttribute('id');

                  var navigationLink = sectionIdTonavigationLink[id];

                  
                  // console.log("Section at the top is: ", id, scrollPosition, section.offsetTop, sectionIdTonavigationLink)
                  // console.log("SrollPos+sectOffset ", section.offsetTop + section.clientHeight)
                  // console.log("SrollPos ", scrollPosition, ' secOffset', section.offsetTop, 'sectHeight', section.clientHeight)


                  if (!hasClass(navigationLink, 'active')) {
                      
                      for (let i=0; i<$navigationLinks.length; i++){
                        if ($navigationLinks[i] === navigationLink) {

                          navigationLink.parentNode.classList.add('active');
                        }else {
                          $navigationLinks[i].parentNode.classList.remove('active');
                        }
                      }
                  }
                  return false;
              }
          });
        }
      
        
      
        window.onscroll = throttle( optimized, 100 );
        
      
        
    











    this.setState({activeleft: this.props.location.pathname.slice(1,).split('/').pop()});
  }
  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps !==this.props){
    this.setState({activeleft: this.props.location.pathname.slice(1,).split('/').pop()});

    }
  }
  

  render() {
    let paths = this.props.location.pathname.slice(1,).split('/')
    let breadcrumbs = [];
    for (let i = 0; i < paths.length; i++) {
      breadcrumbs.push(
      <span key={i}>
        <a  href={paths.slice(0,i+1).join('/')}>{paths[i]}</a>
        {i<paths.length-1?<span className='breadcrumb-divider'> > </span>:""}
      </span>)
      // breadcrumbs.push(<i>></i>)
    }

    return (
      <div className='documentation'>
        <TopNav/>

        <div className='doc-body'>
          

          <div className='doc-left-nav'>
            <div className='nav-group'><a href='/documentation/checkout'>Payments</a></div>
            <div className='group-links'>
              <a className={this.state.activeleft==='overview'?'active':''} href='/documentation/checkout/overview' >Overview</a>
              <a className={this.state.activeleft==='integration'?'active':''} href='/documentation/checkout/integration'>API Integration</a>
              <a className={this.state.activeleft==='web-sdk'?'active':''} href='/documentation/checkout/web-sdk'>Web SDK</a>
              <a className={this.state.activeleft==='ios-sdk'?'active':''} href='/documentation/checkout/ios-sdk'>iOS SDK</a>
              <a className={this.state.activeleft==='android-sdk'?'active':''} href='/documentation/checkout/android-sdk'>Android SDK</a>
              <a className={this.state.activeleft==='resultscode'?'active':''} href='/documentation/checkout/resultscode'>Results code</a>
            </div>
          </div>

          <div className='doc-right-nav'>
            <div className='page-nav active'>
              <a href='#overview'>On this page</a>
            </div>
            <div className='page-nav '>
              <a href='#howitworks'>How it works</a>
            </div>
            <div className='page-nav '>
              <a href='#getstarted'>Get started</a>
            </div>
          </div>
          
          <div className='doc-content'>
          <div className='breadcrumbs'>{breadcrumbs}</div>
            <div className='breadcrambs'></div>

            <div id='overview' className='doc-section'>
              <h1>
                Payments overview
              </h1>
              <p>
                There are two ways you can integration Spektra Checkout into your website or
                mobile app.
              </p>
              <p>
                The quickest is to integrate through our SDKs for
                <a>
                  Web</a>,
                <a>
                  iOS</a>
                and
                <a>Android</a>. Set up your server, add a few lines of code to your website or
                app, and you're up and running.</p>
              <p>
                If you want to use your own Ul or create custom payment flows, use the Checkout
                API integration. If you are a PCI SAQ-D merchant, you can also use the API to
                collect your shopper's card data Either way</p>
              <p>
                your integration can accept all the
                <a>payment methods</a>
                offered by Spektra, and support 3D Secure
              </p>
            </div>

            <div id='howitworks' className='doc-section'>
              <h1>
                How To Accept Payments With Spektra
              </h1>
              <p>
                There are two ways you can integration Spektra Checkout into your website or
                mobile app.
              </p>
              <p>
                The quickest is to integrate through our SDKs for
                <a>
                  Web</a>,
                <a>
                  iOS</a>
                and
                <a>Android</a>. Set up your server, add a few lines of code to your website or
                app, and you're up and running.</p>
              <p>
                If you want to use your own Ul or create custom payment flows, use the Checkout
                API integration. If you are a PCI SAQ-D merchant, you can also use the API to
                collect your shopper's card data Either way</p>
              <p>
                your integration can accept all the
                <a>payment methods</a>
                offered by Spektra, and support 3D Secure
              </p>
            </div>

            <div id='getstarted' className='doc-section'>
              <h1>
                Get Started
              </h1>
              <p>
                Integrate checkout into your website or app. Use our step-by-step guide to get
                started:
              </p>

              <Grid container justify="center" spacing={16}>
                <Grid item xs={6}>
                  <div className='doc-cat-card'>
                    <h1>API</h1>
                    <p>Build your own checkout experience and tools.</p>
                  </div>

                </Grid>
                <Grid item xs={6}>
                  <div className='doc-cat-card'>
                    <h1>Web SDK</h1>
                    <p>Quickly intergrate checkout into your website.</p>
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div className='doc-cat-card'>
                    <h1>iOS SDK</h1>
                    <p>Quickly intergrate checkout into your iOS app.</p>
                  </div>

                </Grid>
                <Grid item xs={6}>
                  <div className='doc-cat-card'>
                    <h1>Android SDK</h1>
                    <p>Quickly integrate Checkout into your Android app.</p>
                  </div>
                </Grid>

              </Grid>
            </div>
            <div id="contactsupport" className='doc-footer'>
              <div className='doc-footer-1'>
              <p>Have questions? Ask for help by contacting our
                <a>Support</a>
                team.</p>
            </div>
            </div>
            <div id='docfooter' className='doc-footer'>
            <div className='doc-footer-copy'>
            <p>Spektra Inc. &copy; 2018</p>
            </div>
          </div>
          </div>
        </div>
      </div>
    );
  }

  
}

export default withRouter(Documentation);
