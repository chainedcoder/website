import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import './App.css';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Error404 from './pages/Error404';
import Documentation from './pages/Documentation.js';

// const Routers =
ReactDOM.render(
  <Router>
  <div>
    <Route exact path='/' component={App}/>
    <Route path="/error" component={Error404}/>
    <Route exact path="/documentation/" component={Documentation}/>
    <Route  exact path="/documentation/checkout" component={Documentation}/>
    <Route exact path="/documentation/checkout/support" component={Documentation}/>
    <Route exact path="/documentation/checkout/API Explore" component={Documentation}/>
    <Route exact path="/documentation/checkout/overview" component={Documentation}/>
    <Route exact path="/documentation/checkout/integration" component={Documentation}/>
    <Route exact path="/documentation/checkout/web-sdk" component={Documentation}/>
    <Route exact path="/documentation/checkout/ios-sdk" component={Documentation}/>
    <Route exact path="/documentation/checkout/android-sdk" component={Documentation}/>
    <Route exact path="/documentation/checkout/Resultscode" component={Documentation}/>
  </div>
</Router>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls. Learn
// more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
