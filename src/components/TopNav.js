import React from 'react';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  sectionDesktop: {
    display: 'none',
    [
      theme
        .breakpoints
        .up('md')
    ]: {
      display: 'flex',
      width: '100%',
      justifyContent: 'space-between'
    }
  },
  sectionMobile: {
    display: 'flex',
    width: '100%',
    [
      theme
        .breakpoints
        .up('md')
    ]: {

      display: 'none'
    }
  },
})
class TopNav extends React.Component {
  constructor() {
    super();
    this.state = {
      scrolled: false,
      Four0four: false,
      section:'',
    };
  }

  

  render() {
    const {classes} = this.props;

    const homeNav =  <div className='nav-left'>

    <a href='#features' className={this.state.section==='features'? 'active-nav':''}>
      <div className='nav-link'>Features</div>
    </a>
    <a href='#pricing' className={this.state.active==='pricing'? 'active-nav':''}><div className='nav-link'>Pricing</div></a>
    <a href='/documentation' className={this.state.active==='documentation'? 'active-nav':''}><div className='nav-link'>Documentation</div></a>

  </div>

    return (
      <div
        className={this.state.scrolled
        ? 'topnav topnav-white'
        : 'topnav topnav-clear'}>
        <Grid container spacing={0}>
          <div className={classes.sectionDesktop}>
            <Grid item sm={9} lg={9} md={9}>
              <Grid container spacing={0}>
                <Grid item sm={4} lg={4} md={4}>
                <a href='/'><div
                    className={this.state.scrolled
                    ? 'purple-logo'
                    : 'white-logo'}></div></a>
                </Grid>

                <Grid item sm={6} lg={6} md={6}>

                  {this.state.four0four
                    ? <div className='nav-left'></div>
                    :(
                      homeNav
                    )}
                </Grid>
              </Grid>
            </Grid>
            <Grid className="" item sm={3} lg={3} md={3}>
              <div className='nav-right-btn'>
                <Button
                  className={this.state.scrolled
                  ? 'grey'
                  : 'white'}>Log In</Button>
                <Button color='inherit' className='signup-btn blue-button caps'>Sign up</Button>
              </div>
            </Grid>
          </div>
          <div className={classes.sectionMobile}>
            <Grid item xs={12} md={12} sm={12}>
              <div className='nav-right-btn'>
                <Button
                  className={this.state.scrolled
                  ? 'grey'
                  : 'white'}>Log In</Button>
                <Button color='inherit' className='signup-btn blue-button caps'>Sign up</Button>
              </div>
            </Grid>
          </div>

        </Grid>
      </div>
    );
  }

  componentDidMount() {
    this.setState({scrolled: this.props.scrolled});
  }
  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps !== this.props) {
      const four0four = this.props.four0four ? true:false
      this.setState({scrolled: this.props.scrolled, four0four: four0four, active: this.props.active, section: this.props.section})
    }
  }

}

TopNav.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TopNav);
