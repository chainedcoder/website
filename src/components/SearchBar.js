import React, { Component } from 'react'
import PropTypes from 'prop-types'
// import IconButton from '@material-ui/core/IconButton'
// import Paper from '@material-ui/core/Paper'
// import CloseIcon from '@material-ui/icons/close'
// import { grey500 } from '@material-ui/styles/colors'
import InputBase from '@material-ui/core/InputBase';

const getStyles = (props, state) => {
  const {disabled, iconButtonStyle} = props
  const {value} = state
  const nonEmpty = value.length > 0

  return {
    root: {
      height: 34,
      justifyContent: 'flex-end',
      minWidth: '250px', 
      width: 300,  
      background: "#f6f9fc",
        },
    searchIcon: {
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      },
    
    iconButtonClose: {
      style: {
        opacity: !disabled ? 0.54 : 0.38,
        transform: nonEmpty ? 'scale(1, 1)' : 'scale(0, 0)',
        transition: 'transform 200ms cubic-bezier(0.4, 0.0, 0.2, 1)',
        ...iconButtonStyle
      },
      iconStyle: {
        opacity: nonEmpty ? 1 : 0,
        transition: 'opacity 200ms cubic-bezier(0.4, 0.0, 0.2, 1)'
      }
    },
    iconButtonSearch: {
      style: {
        opacity: !disabled ? 0.54 : 0.38,
        transform: nonEmpty ? 'scale(0, 0)' : 'scale(1, 1)',
        transition: 'transform 200ms cubic-bezier(0.4, 0.0, 0.2, 1)',
        marginRight: -48,
        ...iconButtonStyle
      },
      iconStyle: {
        opacity: nonEmpty ? 0 : 1,
        transition: 'opacity 200ms cubic-bezier(0.4, 0.0, 0.2, 1)'
      }
    },
    input: {
      width: '100%'
    },
    searchContainer: {
      margin: 'auto 16px',
      width: '100%'
    }
  }
}


export default class SearchBar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      focus: false,
      value: this.props.value,
      active: false
    }
    this.searchField = React.createRef();
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.value !== nextProps.value) {
      this.setState({...this.state, value: nextProps.value})
    }
  }

  focus () {
    this.autoComplete.focus()
  }

  blur () {
    this.autoComplete.blur()
  }

  handleFocus () {
    this.setState({focus: true})
  }

  handleBlur () {
    this.setState({focus: false})
    if (this.state.value.trim().length === 0) {
      this.setState({value: ''})
    }
  }

  handleInput (e) {
    this.setState({value: e})
    this.props.onChange(e)
  }

  handleCancel () {
    this.setState({active: false, value: ''})
    this.props.onChange('')
  }

  handleKeyPressed (e) {
    if (e.charCode === 13) {
      this.props.onRequestSearch()
    }
  }

  render () {
    const styles = getStyles(this.props, this.state)
    const {value} = this.state
    const {
      onRequestSearch,
      style,
      // ...inputProps
    } = this.props

    return (
      <div
        style={{
          ...styles.root,
          ...style
        }}
      >
        <div style={styles.searchContainer}>
        <InputBase style={styles.input} defaultValue="" placeholder='search...' ref={this.searchField}
            onBlur={() => this.handleBlur()}
            searchtext={value}
            onChange={(e) => this.handleInput(e)}
            onKeyPress={(e) => this.handleKeyPressed(e)}
            onFocus={() => this.handleFocus()}/>
        </div>
        {/* <IconButton
          onClick={onRequestSearch}
          iconStyle={styles.iconButtonSearch.iconStyle}
          style={styles.iconButtonSearch.style}
          disabled={disabled}
        >
          
        </IconButton> */}
        <div onClick={onRequestSearch} className={styles.searchIcon + " search-icon"}></div>

        {/* <IconButton
          onClick={() => this.handleCancel()}
          iconStyle={styles.iconButtonClose.iconStyle}
          style={styles.iconButtonClose.style}
          disabled={disabled}
        >
          {closeIcon}
        </IconButton> */}
      </div>
    )
  }
}

SearchBar.defaultProps = {
//   closeIcon: <CloseIcon color={grey500} />,
  dataSource: [],
  dataSourceConfig: {text: 'text', value: 'value'},
  disabled: false,
  hintText: 'Search',
  spellCheck: false,
  value: ''
}

SearchBar.propTypes = {
  dataSource: PropTypes.array,
  dataSourceConfig: PropTypes.object,
  disabled: PropTypes.bool,
  hintText: PropTypes.string,
  iconButtonStyle: PropTypes.object,
  onChange: PropTypes.func.isRequired,
  onRequestSearch: PropTypes.func.isRequired,
  spellCheck: PropTypes.bool,
  style: PropTypes.object,
  value: PropTypes.string
}
