import React from 'react';
import Grid from '@material-ui/core/Grid';

class Footer extends React.Component {
  constructor() {
    super();
    this.state = { someKey: 'someValue' };
  }

  render() {
    return (
        <div>
          
          <div className='footer-links'>

            <Grid container justify="center" spacing={16}>
              <Grid item sm={3} lg={3} md={3} xs={12}>
                <div className='footer-logo'></div>
                <h3>Spektra Inc.</h3>
                <p>New York, Nairobi, Accra</p>

              </Grid>
              <Grid item sm={3} lg={3} md={3} xs={12}>
                <h2>QUICK LINKS</h2>
                <a href='/signup'>Create a new Account</a>
                <a href='/documentation'>Documentation</a>
                <a href='/support'>My Account</a>

              </Grid>
              <Grid item sm={3} lg={3} md={3} xs={12}>
                <h2>providers</h2>
                <a href='#safaricom'>Safaricom</a>
                <a href='#mtn'>MTN</a>
                <a href='#airtel'>Airtel</a>
                <a href='#tigo'>Tigo</a>
                <a href='#vodafone'>Vodafone</a>

              </Grid>
              <Grid item sm={3} lg={3} md={3} xs={12}>
                <h2>Need help?</h2>
                <a href='#contactus'>Contact Us</a>
                <a href='#support'>Support</a>
                <a href='#blog'>Blog</a>
                <a href='#howto'>How-To</a>
                <a href='#forum'>Forum</a>

              </Grid>

            </Grid>
          </div>
          <div className='footer-copy'>
            <div className='pull-left'>Privacy & terms</div>
            <div className='pull-right'>Spektra Inc &copy; 2018 </div>
          </div>

        </div>
    );
  }

  componentDidMount() {
    this.setState({ someKey: 'otherValue' });
  }
}

export default Footer;
