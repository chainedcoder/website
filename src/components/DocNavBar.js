import React from 'react';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import SearchBar from '../components/SearchBar';
import {withRouter} from 'react-router-dom';

const styles = theme => ({
  centerFlexItem: {
    display: "-webkit-inline-flex",
    WebkitBoxOrient: "vertical",
    WebkitBoxDirection: "normal",
    WebkitFlexDirection: "column",
    WebkitBoxPack: "center",
    WebkitFlexPack: "center",
    WebkitJustifyContent: "center",
    WebkitFlexAlign: "center",
    WebkitAlignItems: "center",
    verticalAlign: "top"
  },
  sectionDesktop: {
    display: 'none',
    [
      theme
        .breakpoints
        .up('md')
    ]: {
      display: 'flex',
      width: '100%',
      justifyContent: 'space-between'
    }
  },
  sectionMobile: {
    display: 'flex',
    width: '100%',
    [
      theme
        .breakpoints
        .up('md')
    ]: {

      display: 'none'
    }
  }
})
class TopNav extends React.Component {
  constructor() {
    super();
    this.state = {
      scrolled: false,
      Four0four: false,
      section: ''
    };
  }

  render() {
    const {classes} = this.props;

    const docNav = <div className='nav-left'>

      <a
        href='/documentation'
        className={this.state.active === 'documentation'
        ? 'active-nav'
        : ''}>
        <div className='nav-link'>Documentation</div>
      </a>
      <a
        href='/Support'
        className={this.state.active === 'support'
        ? 'active-nav'
        : ''}>
        <div className='nav-link'>Support</div>
      </a>
      {/* <a
        href='/API Explore'
        className={this.state.active === 'apiexplore'
        ? 'active-nav'
        : ''}>
        <div className='nav-link'>API Explore</div>
      </a> */}
    </div>

    return (
      <div className='topnav topnav-white'>
        <Grid container spacing={0}>
          <div className={classes.sectionDesktop}>
            <Grid item sm={11} lg={11} md={11}>
              <Grid container spacing={0}>
                <Grid item sm={3} lg={3} md={3}><a href='/'>
                  <div className='purple-logo doc-logo'></div></a>
                </Grid>

                <Grid item sm={5} lg={5} md={5}>

                  {docNav}
                </Grid>
                <Grid className="" item sm={3} lg={3} md={3}>
                  <div
                    style={{
                    display: 'flex',
                    height: '100%'
                  }}>
                    <div className={classes.centerFlexItem}><SearchBar
                      onChange={() => console.log('onChange')}
                      onRequestSearch={() => console.log('onRequestSearch')}
                      onClear={() => console.log('onClear')}
                      style={{
                          margin: '0 auto',
                          maxWidth: 400
                        }}/></div>
                  </div>

                </Grid>
              </Grid>
            </Grid>

          </div>
          <div className={classes.sectionMobile}>
            <Grid item xs={12} md={12} sm={12}>
              <div className='nav-right-btn'>
                <Button className='grey'>Log In</Button>
                <Button color='inherit' className='signup-btn blue-button caps'>Sign up</Button>
              </div>
            </Grid>
          </div>

        </Grid>
      </div>
    );
  }

  componentDidMount() {
    this.setState({scrolled: this.props.scrolled});
    console.log(this.props)
  }
  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps !== this.props) {
      const four0four = this.props.four0four
        ? true
        : false
      this.setState({scrolled: this.props.scrolled, four0four: four0four, active: this.props.active, section: this.props.section})
    }
  }

}

TopNav.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(withRouter(TopNav));
