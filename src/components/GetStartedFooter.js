import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';


class GetStartedFooter extends React.Component {
  constructor() {
    super();
    this.state = { someKey: 'someValue' };
  }

  render() {
    return (
        <div className='footer-highlite'>
            <Grid container spacing={0}>
              <Grid item sm={6} lg={6} md={6} xs={12}>
                <div className='footer-left'>
                  <h1>Ready to Get started?</h1>
                  <p>Create a free Spektra account to begin.</p>
                  <div className='highlite-buttons'>
                    <Button color='inherit' className='blue-button'>Create An Account</Button>
                  </div>
                </div>

              </Grid>
              <Grid item sm={6} lg={6} md={6} xs={12}>
                <div className='footer-right'>
                  <h1>And Then What?</h1>
                  <p>Start integrating Spektra. It takes a few minutes.</p>
                  <div className='highlite-buttons'>
                    <Button color='inherit' className='blue-button'>Read Our Documentaion</Button>
                  </div>

                </div>
              </Grid>

            </Grid>
          </div>
    );
  }

  componentDidMount() {
    this.setState({ someKey: 'otherValue' });
  }
}

export default GetStartedFooter;
