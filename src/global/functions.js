export function throttle(fn, wait) {
  var time = Date.now();
  return () => {
    if ((time + wait - Date.now()) < 0) {
      fn();
      time = Date.now();
    }
  }
}

export function hasClass(target, className) {
  return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
}

export function offset(elem) {
  var box = {
    top: 0,
    left: 0
  };


  let win = window;
  return {
    top: box.top + (win.pageYOffset || document.scrollTop) - (document.clientTop || 0),
    left: box.left + (win.pageXOffset || document.scrollLeft) - (document.clientLeft || 0)
  };
}

export const retry = (maxRetries, par, fn) => {
  setTimeout(() => {
    console.log(par, '**', fn)
    return fn(par).catch((err) => {
      if (maxRetries <= 0) {
        throw err;
      }
      return retry(maxRetries - 1, fn);
    });

  }, 100)

}

export function scrollTop() {
   return window.scrollY || window.pageYOffset || document.body.scrollTop || (document.documentElement && document.documentElement.scrollTop || 0)
}

export async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}