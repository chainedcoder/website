import React, {Component} from 'react';
// import logo from './logo.svg';
import TopNav from './components/TopNav'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import clock from './images/IntegrationIcon.svg'
import disputeIcon from './images/DisputesIcon.svg'
import crossborderIcon from './images/CrossBorderIcon.svg'

class App extends Component {
  constructor() {
    super();
    this.state = {
      scrolled: false,
      pageOpen: 'home'
    };

  }

  throttle = (fn, wait) => {
    var time = Date.now();
    return () => {
      if ((time + wait - Date.now()) < 0) {
        fn();
        time = Date.now();
      }
    }
  }
  

  checkVisible = (id) => {
    try{

      const el = document.getElementById(id);
  
      var rect = el.getBoundingClientRect();
      var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
      
      console.log(id, !(rect.bottom < 0 || rect.top - viewHeight >= 0))
      return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
    } catch {
      return false;

    }
  }

  componentDidMount = () => {
    const handleNav = (body) => {
      

      if ((document.body.scrollTop > 10 || window.pageYOffset > 10 || document.documentElement.scrollTop > 10)) {
        if (!this.state.scrolled) {

          this.setState({scrolled: true})
        }
      } else {
        this.setState({scrolled: false})

      }
      if(this.checkVisible('features')){
        if(this.state.pageSecOpen !== 'features'){

          this.setState({pageSecOpen:'features'});
        }
      } else {
        if( this.state.pageSecOpen === 'features'){
          this.setState({pageSecOpen:''});
        }
      }
    }
    window.onscroll = this.throttle(handleNav, 50)

    const gotopage = (page) => {
      this.setState({pageOpen: page})

    }

    document
      .querySelectorAll('a[href^="#"]')
      .forEach(anchor => {
        anchor
          .addEventListener('click', function (e) {
            e.preventDefault();
            const link = this
              .href
              .split('#')[1]

            console.log(link)
            switch (link) {
              case 'features':
                gotopage('home');
                break;
              case 'pricing':
                gotopage('pricing');
                break;
              case 'documentation':
                gotopage('documentaion');
                break;

              default:
                break

            }

            const retry = (maxRetries, el, fn) => {
              setTimeout(() => {
                return fn(el).catch((err) => {
                  if (maxRetries <= 0) {
                    throw err;
                  }
                  return retry(maxRetries - 1, fn);
                });

              }, 100)

            }

            async function scroll(el) {
              document
                .querySelector(el.getAttribute('href'))
                .scrollIntoView({behavior: 'smooth'});
            }
            retry(100, this, scroll)

          });
      });

  }
  componentWillUnmount = () => {
    document.removeEventListener('scroll', this.throttle)
  }

  render() {

    const homePage = <div>
      <div className='landing'>
        <Grid container spacing={0}>
          <Grid item sm={6} lg={6} md={6} xs={12}>
            <div className='left-landing'>
              <div className='highlite-text'>
                <h1>Accept Mobile Money Payments Instantly.</h1>
                <p>Getting paid for your online business shouldn't be rocket science. Spektra
                  makes it easy for you to accept Mobile Money payments instantly so you can focus
                  on running your business.</p>
              </div>

              <div className='highlite-buttons'>
                <Button color='inherit' className='blue-button'>Create Account</Button>
              </div>

              <div className='landing-scroll'>
                <a href='#experience'>
                  <Button color='inherit' className='scroll-icon scroll-btn caps'>scroll</Button>
                </a>
              </div>

            </div>
          </Grid>

          <Grid item sm={6} lg={6} md={6} xs={12}>
            <div className='right-landing'></div>
          </Grid>
        </Grid>

      </div>

      <div id='experience' className='experience-container'>
        <Grid container spacing={0}>
          <Grid item sm={6} lg={6} md={6} xs={12}>
            <div className='experience'>
              <div>
                <h1>A seamless checkout experience</h1>

                <p>We've built the first Universal Mobile Money Online Checkout to make your
                  payments a breeze. It requires just a single step and works on the web, mobile
                  and in-apps. No need to bog down your customers with lengthy instructions.</p>
              </div>
              <div className='highlite-buttons'>
                <Button color='inherit' className='blue-button'>Get Started</Button>
              </div>

            </div>
          </Grid>
          <Grid item sm={6} lg={6} md={6} xs={12}>
            <div className='experience-image'></div>
          </Grid>
        </Grid>
      </div>

      <div id='features' className='features-section'>
        <div
          style={{
          height: '100px',
          width: '100vw',
          display: 'hidden'
        }}></div>

        <Grid container justify="center" spacing={16}>
          <Grid item sm={3} lg={3} md={3} xs={12}>

            <div className='card card-white '>
              <div >
                <img className='icon' src={`${clock}`} alt='Time Icon'></img>
              </div>
              <h1>Quick Integration</h1>
              <p>
                It literally takes 10 minutes to get started with just a few lines of code.
                Accept payments from multiple Mobile Money providers with a single integration.
                Want to build your own stuff? Unleash your own creative prowess with our APIs.
              </p>
              <div className='card-link'>
                <a href='#documentation'>View Documentation ></a>
              </div>
            </div>
          </Grid>
          <Grid item sm={3} lg={3} md={3} xs={12}>
            <div className='card blue-card'>
              <div >
                <img className='icon' src={`${disputeIcon}`} alt='disputes icon'></img>
              </div>
              <h1>Resolve Disputes</h1>
              <p>
                Regain the trust of your customers and resolve disputes instantly by issuing
                refunds to your customers right from your dashboard.
              </p>
              <div className='card-link'>
                <a href='#dashboard'>View Dashboard ></a>
              </div>

            </div>
          </Grid>
          <Grid item sm={3} lg={3} md={3} xs={12}>
            <div className='card card-white '>
              <div >
                <img className='icon' src={`${crossborderIcon}`} alt='cross border icon'></img>
              </div>
              <h1>Cross-Border Payments</h1>
              <p>Have customers outside of your country? Fret not, we make it easy to accept
                payments from multiple countries and currencies with no additional effort nor
                integration.
              </p>
              <div className='card-link'>
                <a href='#payments'>Accept Payments ></a>
              </div>

            </div>
          </Grid>
        </Grid>
      </div>

    </div>

    const pricingPage =
    <div id='pricing' className='pricing-page'>
      <div className='pricing-highlite'>
        <h1>We make money when you make money</h1>
        <p>Spektra is free to integrate, we make money only when you receive a succesful transaction. Our competitive rates below.</p>
      </div>

      <Grid container justify="center" spacing={0}>
        <Grid item sm={6} lg={6} md={6} xs={12}>
          <div className='pricing-card'>
          <div className='top'></div>
            <div className='pricing-rate'>
              <h2>Payments</h2>
              <Grid container justify="center" spacing={16}>
                <Grid item sm={6} lg={6} md={6} xs={12}>
                  <div className='rate'>2.9%</div>
                </Grid>
                <Grid item sm={5} lg={5} md={5} xs={12}>
                <div className='rate-text'><p>Per succesful transaction</p></div>
                </Grid>
              </Grid>
              <p>Accept payments from multiple countries, currencies and providers.</p>
            </div>
            <div className='pricing-features'>
              <div className='feature-item'>
                <div className='tick-icon'></div>
                <div className='feature-text'>
                  <p>No withdrawal fees</p>
                </div>
              </div>

              <div className='feature-item'>
                <div className='tick-icon'></div>
                <div className='feature-text'>
                  <p>No extra costs for cross-border payments</p>
                </div>
              </div>

              <div className='feature-item'>
                <div className='tick-icon'></div>
                <div className='feature-text'>
                  <p>Notification on successful transactions</p>
                </div>
              </div>

              <div className='feature-item'>
                <div className='tick-icon'></div>
                <div className='feature-text'>
                  <p>No minimum amount</p>
                </div>
              </div>

            </div>
            <div className='pricing-footer'>
                Get Started
              </div>
          </div>
        </Grid>

        <Grid item sm={6} lg={6} md={6} xs={12}>
          <div className='pricing-card'>
            <div className='top'></div>
            <div className='pricing-rate'>
              <h2>Payouts</h2>
              <Grid container justify="center" spacing={32}>
                <Grid item sm={6} lg={6} md={6} xs={12}>
                  <div className='rate'>0.35%</div>
                </Grid>
                <Grid item sm={5} lg={5} md={5} xs={12}>
                <div className='rate-text'><p>Per succesful transaction</p></div>
                </Grid>
              </Grid>
              <p>Send money to any Mobile Number in multiple countries and currencies.</p>
            </div>
            <div className='pricing-features'>
              <div className='feature-item'>
                <div className='tick-icon'></div>
                <div className='feature-text'>
                  <p>Payout instantly from your dashboard</p>
                </div>
              </div>

              <div className='feature-item'>
                <div className='tick-icon'></div>
                <div className='feature-text'>
                  <p>No minimum amount</p>
                </div>
              </div>

              <div className='feature-item'>
                <div className='tick-icon'></div>
                <div className='feature-text'>
                  <p>Receiver identity verification</p>
                </div>
              </div>

              <div className='feature-item'>
                <div className='tick-icon'></div>
                <div className='feature-text'>
                  <p>No upfront deposit needed</p>
                </div>
              </div>

            </div>
            <div className='pricing-footer'>
                Get Started
              </div>
          </div>
        </Grid>

      </Grid>

    </div>

    return (
    <div className="App">
      <TopNav
        active={this.state.pageOpen}
        section={this.state.pageSecOpen}
        scrolled={this.state.pageOpen === 'home'
        
        ? this.state.scrolled
        : true}/> {this.state.pageOpen === 'home'
        ? homePage
        : ''}
      {this.state.pageOpen === 'pricing'
        ? pricingPage
        : ''}

      <footer className='footer-section'>
        <div className='footer-highlite'>
          <Grid container spacing={0}>
            <Grid item sm={6} lg={6} md={6} xs={12}>
              <div className='footer-left'>
                <h1>Ready to Get started?</h1>
                <p>Create a free Spektra account to begin.</p>
                <div className='highlite-buttons'>
                  <Button color='inherit' className='blue-button'>Create An Account</Button>
                </div>
              </div>

            </Grid>
            <Grid item sm={6} lg={6} md={6} xs={12}>
              <div className='footer-right'>
                <h1>And Then What?</h1>
                <p>Start integrating Spektra. It takes a few minutes.</p>
                <div className='highlite-buttons'>
                  <Button color='inherit' className='blue-button'>Read Our Documentaion</Button>
                </div>

              </div>
            </Grid>

          </Grid>
        </div>
        <div className='footer-links'>

          <Grid container justify="center" spacing={16}>
            <Grid item sm={3} lg={3} md={3} xs={12}>
              <div className='footer-logo'></div>
              <h3>Spektra Inc.</h3>
              <p>New York, Nairobi, Accra</p>

            </Grid>
            <Grid item sm={3} lg={3} md={3} xs={12}>
              <h2>QUICK LINKS</h2>
              <a href='/signup'>Create An Account</a>
              <a href='/documentation'>Documentation</a>
              <a href='/support'>Log In</a>

            </Grid>
            <Grid item sm={3} lg={3} md={3} xs={12}>
              <h2>providers</h2>
              <a href='#safaricom'>Safaricom</a>
              <a href='#MTN'>MTN</a>
              <a href='#airtel'>Airtel</a>
              <a href='#tigo'>Tigo</a>
              <a href='#vodafone'>Vodafone</a>

            </Grid>
            <Grid item sm={3} lg={3} md={3} xs={12}>
              <h2>Need help?</h2>
              <a href='#contactus'>Contact Us</a>
              <a href='#support'>Support</a>
              <a href='#blog'>Blog</a>
              <a href='#howto'>How-To</a>
              <a href='#forum'>Forum</a>

            </Grid>

          </Grid>
        </div>
        <div className='footer-copy'>
          <div className='pull-left'>Privacy & terms</div>
          <div className='pull-right'>Spektra Inc. &copy; 2018
          </div>
        </div>

      </footer>

    </div>
        )
      }
    }
    
    export default App;